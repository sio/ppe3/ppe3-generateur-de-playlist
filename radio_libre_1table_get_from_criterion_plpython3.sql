-- Function: radio_libre.get_from_criterion(character varying, character varying, integer)

-- DROP FUNCTION radio_libre.get_from_criterion(character varying, character varying, integer);

CREATE OR REPLACE FUNCTION radio_libre.get_from_criterion(
    criterion character varying,
    filter character varying,
    duration integer)
  RETURNS SETOF radio_libre.morceaux AS
$BODY$
import random
output = []
if criterion in ['titre', 'album', 'artiste', 'genre', 'sousgenre']:
    prepared_statement = plpy.prepare("SELECT * FROM morceaux WHERE %s ~ $1 ORDER BY random()" % (criterion), ["character varying"])
    records = plpy.execute(prepared_statement, [filter])
    if len(records) > 0:
        end_selecting = False
        duration_sum = 0
        duration_in_seconds = duration * 60
        tuple_counter = 0
        margin = 30
        selected = False
        while duration_sum < (duration_in_seconds - margin):
            current_tuple = records[tuple_counter % len(records)]
            if (current_tuple["duree"] + duration_sum) <= duration_in_seconds:
                output.append(current_tuple)
                duration_sum += current_tuple["duree"]
                selected = True
            tuple_counter = (tuple_counter + 1) % len(records)
            if tuple_counter % len(records) == 0:
                if selected:
                    selected = False
                else:
                    break
        random.shuffle(output)
else:
    plpy.error("No column named '%s' in table morceaux. Please correct your query."
               % (criterion))
return output
$BODY$
  LANGUAGE plpython3u VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION radio_libre.get_from_criterion(character varying, character varying, integer)
  OWNER TO enseignants;
GRANT EXECUTE ON FUNCTION radio_libre.get_from_criterion(character varying, character varying, integer) TO enseignants;
GRANT EXECUTE ON FUNCTION radio_libre.get_from_criterion(character varying, character varying, integer) TO "etudiants-slam";
REVOKE ALL ON FUNCTION radio_libre.get_from_criterion(character varying, character varying, integer) FROM public;
COMMENT ON FUNCTION radio_libre.get_from_criterion(character varying, character varying, integer) IS 'Fonction permettant d''établir une liste d''éléments ''morceaux'' basés sur des critères de sélection fournis en paramètres.';
