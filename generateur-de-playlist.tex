% PPE3 : generateur de playlist, en correlation avec le contexte Radio
% Libre Malraux
%
% Auteur : Gregory DAVID and Geraldine TAYSSE
%%

\documentclass[12pt,a4paper,oneside,titlepage,final]{article}

\usepackage{style/layout}%
\usepackage{style/commands}%
\usepackage{style/glossaire}%
\usepackage{bashful}%

\newcommand{\MONTITRE}{Générateur de playlist}
\newcommand{\MONSOUSTITRE}{Cahier des charges fonctionnel}
\newcommand{\DISCIPLINE}{\gls{PPE} \No 3}

\usepackage[%
pdftex,%
pdftitle={\MONTITRE},%
pdfauthor={Grégory DAVID et Géraldine TAYSSE},%
pdfsubject={\MONSOUSTITRE},%
colorlinks,%
]{hyperref}

\lstset{%
  basicstyle={\scriptsize\ttfamily},%
}%

\author{%
  \begin{minipage}{1.0\linewidth}
      \begin{flushright}
        version \texttt{\gitFirstTagDescribe{}}\\en date du
        \gitAuthorIsoDate{}\\~\hrule{}
      \end{flushright}
  \end{minipage}\\
  \begin{minipage}{0.5\linewidth}
      \noindent\href{mailto:groolot@groolot.net}{Grégory \textsc{David}}\\
      \noindent\href{mailto:geraldine.taysse@ac-nantes.fr}{Géraldine
        \textsc{Taysse}}
  \end{minipage}
  \begin{minipage}{0.5\linewidth}
      \begin{flushright}
          \Glsentrydesc{LYCMALRAUX}\\
          3 rue de Beau Soleil\\
          72700 Allonnes\\
      \end{flushright}
  \end{minipage}
}

\title{%
  \begin{flushright}
      \noindent{\Huge {\bf \MONTITRE}} \\
      \noindent{\huge \MONSOUSTITRE} \\
      \noindent{\large \DISCIPLINE} \\
  \end{flushright}
}

\begin{document}
\begin{titlepage}
  % Page de titre
  \maketitle
\end{titlepage}

% Copyright
\include{copyright}

% Contenu
\HEADER{\MONTITRE}{\DISCIPLINE} \rhead{\MAILGREGORY\\Géraldine
  \bsc{Taysse}}%

\part{Contexte}
\label{sec:contexte}
Une équipe du \gls{LYCMALRAUX} gère, maintient et anime la Radio Libre
de l'établissement nommée : \emph{radio6mic}.

La nature des morceaux qui sont écoutés par les auditeurs provient du
travail du \emph{programmateur radio} dont la responsabilité est de
concevoir l'enchaînement musical diffusé.

Pour cela, le \emph{programmateur radio} utilise un logiciel de
lecture sonore \emph{CMus*}. Il ajoute à la main tous les morceaux
qu'il souhaite diffuser. Une fois ajoutés dans la \emph{liste de
  lecture} de \emph{CMus*}, le \emph{programmateur radio} ordonne les
morceaux selon la dynamique qu'il souhaite donner, et enfin il
sauvegarde cette \emph{liste de lecture} (\emph{playlist}) dans le
format \emph{M3U} et le stocke dans le système de fichier à l'endroit
défini par l'équipe de la radio
(\texttt{/radio/base-audio/playlists}).

\begin{quote}
  \emph{Tout cela est bien fastidieux}, se dit le \emph{programmateur
    radio}.
\end{quote}

Non content de ce dur labeur, le \emph{programmateur radio} en appel
aux compétences d'un binôme de développeurs pour lui offrir un outil
de génération automatique de \emph{playlist}.

\part{Projet}\label{sec:projet}
L'objectif global est de fournir à l'organisation un outil de
génération de \emph{playlist} musicale au format initial en
M3U\footnote{voir
  \url{http://forums.winamp.com/showthread.php?threadid=65772}} et
XSPF\footnote{voir \url{http://xspf.org/xspf-v1.html}} afin de
correspondre aux attentes techniques de \emph{Cmus*}.

Par ailleurs, l'avantage de ces formats de \emph{playlist} est qu'ils
sont standardisés et ouverts, et sont ainsi lisibles depuis la
majorité des lecteurs multimedia.

\section{Listes exhaustives des éléments et
  contraintes}\label{sec:elementscontraintes}
L'environnement de fonctionnement est un système d'exploitation
\gls{GNULINUX} dont l'interface graphique est inexistante. La totalité
des applications et programmes employés le sont en interface
\gls{CLI} ou sont des applications \emph{web}.

La seule chose dont nous disposons est la base de données des morceaux
musicaux organisée sur plusieurs tables relationnelles.

Cette base de données est stockée en ligne sur le SGBD
\texttt{PostgreSQL} à l'adresse \texttt{postgresql.bts-malraux72.net}
et écoutant sur le port \texttt{5432}. La base se nomme
\texttt{Cours},le schéma se nomme \texttt{radio\_libre}.

La structure et le jeu de données de la base sont disponibles au
téléchargement sur :
\url{https://framagit.org/sio/ppe3/generateur-de-playlist/blob/master/RadioLibre.sql}.

Vous trouverez dans le \lstlistingname~\vref{lst:SQL} la structure de
la base de données au format PostgreSQL.

L'organisation utilise exclusivement des outils Libres et souhaite que
ses développements soient Libres eux aussi.

\lstinputlisting[caption={Script SQL de la structure de la base de
  données employée}, language={SQL}, linerange={4-46,4567-4600},
label={lst:SQL}]{RadioLibre.sql}

\section{Expression fonctionnelle du besoin}\label{sec:besoins}
\subsection{Fonctions de service
  principales}\label{sec:fonctionsservice}
Fournir un outil permettant de générer des playlists basées sur des
critères de conception :
\begin{itemize}
  \item durée totale de la playlist,
  \item quantité d'un genre,
  \item quantité d'un sous-genre,
  \item quantité d'un artiste,
  \item quantité d'un album,
  \item quantité d'un titre,
  \item quantité d'un élément dont le motif correspond à celui fourni
  selon les règles des expressions rationnelles\footnote{aussi appelée
    \emph{regular expressions}} étendues compatibles PERL (par exemple
  : tous les albums dont le nom contient ``China'' ou ``china'' ou
  ``Chine'' ou ``chine'' : \texttt{.*[Cc]hin[ae].*}, ou alors tous les
  morceaux dont le genre commence par ``Roc'' ou ``roc'' :
  \texttt{\^{}[Rr]oc.*}~).
\end{itemize}

\subsection{Contraintes}\label{sec:fonctionscontraintes}
\subsubsection{Utilisation}\label{sec:licence}
Le programme doit être \emph{Libre} et adopter la licence \emph{GNU
  GPL
  v3}\footnote{\url{http://www.gnu.org/licenses/quick-guide-gplv3.html}},
au sens où il doit offrir les libertés suivantes :
\begin{itemize}
  \item copie,
  \item analyse,
  \item modification,
  \item rediffusion des modifications.
\end{itemize}

Le programme \textbf{ne peut pas devenir \emph{non-libre}}.

\subsubsection{Documentations}\label{sec:documentations}
\begin{itemize}
  \item la \emph{documentation technique} doit être rédigée en
  \emph{anglais} et intégrée au code source auformat \texttt{Doxygen},
  \item la \emph{documentation utilisateur} rédigée en \emph{anglais}
  et correctement illustrée doit être disponible dans les formats
  suivants :
  \begin{itemize}
    \item l'affichage d'une documentation succinte rappelant les
    différentes options disponibles pour le programme, appelée à
    l'aide de l'option \texttt{--help} en paramètre de la ligne de
    commande (utiliser la bibliothèque logicielle adaptée en fonction
    du langage),
    \item une page de manuel visible avec la commande \texttt{man}
    \item une version \texttt{HTML} de la documentation compilée avec
    \texttt{Doxygen},
    \item une version PDF de la documentation compilée avec
    \texttt{Doxygen} et \LaTeX.
  \end{itemize}
\end{itemize}

\subsubsection{\gls{IHM}}\label{sec:IHM}
L'interface du programme est en \gls{CLI}. Ainsi le paramétrage du
fonctionnement se fait par le passage d'options et d'arguments sur la
ligne de commande, par exemple :
\begin{lstlisting}[numbers=none]
./generateur --duration=120 --genre="[Rr]ock",30 --type=M3U
--type=XSPF ma_playlist
\end{lstlisting}

Par ailleurs, l'\gls{IHM} est écrite et documentée en langue
\emph{anglaise}, tandis que nous fournirons (optionellement) des
traductions de l'\gls{IHM} du programme dans la langue
\emph{française}.

\subsubsection{Technologies/Méthodes}\label{sec:technologiques}
Le programme doit utiliser les technologies, validées par le
commanditaire (langages, protocoles, services, méthodologies,
\dots{}), suivantes : voir
\tablename~\vref{tab:technologies.methodes}.

\begin{table}[p]
  \centering
  \begin{tabular}{lp{0.6\textwidth}}
    Langage & \texttt{C/C++} \\
    Langues de l'\gls{IHM} & \emph{anglaise} (originale)\\
            & \emph{française} (traduction optionnelle, par internationalisation
              avec \texttt{gettext}) \\
    Méthodes de conception & Programmation Impérative modulaire
                             en langage \texttt{C} (uniquement pour l'accès aux données) et Orientée Objet
                             en langage \texttt{C++}, avec conception de bibliothèques logicielles
                             lorsque cela est pertinent \\
    SGBD/Stockage des données & PostgreSQL (version 9.6 minimum
                                fournie) \\
    Langage de requêtage & SQL (avec les
                           spécificités du SGBD ; voir à ce sujet
                           \url{http://www.postgresql.org/docs/9.6/static/functions-matching.html} pour l'usage des \emph{regular expressions}) \\
    Journalisation & enregistrement de la trace des actions
                     menées par le programme suivant leur niveau de criticité,
                     effectuée sur la sortie des erreurs du processus \\
    Bibliothèques possibles & \texttt{argp} Intégrée à la \texttt{libc}, offre la gestion
                              des Options et Arguments sur la ligne de commandes :
                              \url{https://www.gnu.org/software/libc/manual/html_node/Argp.html} \\
            & \texttt{TCLAP} Bibliothèque d'en-tête seulement offrant la
              gestion des Options et Arguments sur la ligne de commandes :
              \url{http://tclap.sourceforge.net/} \\
            & \texttt{gettext} Intégrée à la \texttt{libc}, offre les
              modalités de traduction, d'internationalisation :
              \url{http://www.gnu.org/software/gettext/manual/gettext.html} \\
            & \texttt{libxspf} Bibliothèque \texttt{C++} permettant la
              création et l'interprétation des fichiers XSPF :
              \url{https://framagit.org/sio/ppe3/libxspf} \\
            & \texttt{liblog4cxx} Journalisation système et/ou fichier,
              voir la documentation sur
              \url{https://logging.apache.org/log4cxx/latest_stable/} \\
            & \texttt{libpq} Bibliothèque \texttt{C} d'accès au SGBD
              PostgreSQL : \url{https://docs.postgresql.fr/11/libpq.html} \\
    Langue des documentations & \emph{anglaise} \\
    Documentation technique & \texttt{Doxygen} dans le code source des fichiers d'en-tête, au
                              format interprétable par
                              Doxygen\footnote{\url{http://www.stack.nl/~dimitri/doxygen/}} \\
    Documentation utilisateur & visible par la page de manuel standard des sytèmes via la
                                commande \texttt{man}, elle doit respecter les directives données en
                                \vref{sec:documentations} \\
  \end{tabular}
  \caption{Technologies et méthodes}\label{tab:technologies.methodes}
\end{table}

\clearpage%
\section{Livraison (\emph{release})}\label{sec:livraison}

Le projet doit être livré à la date fixée avec le client. Cette
livraison doit être identifiable par un \emph{tag} dans la branche
\texttt{master} du dépôt \glsentrytext{git} nommé : \texttt{v0.1}.

Le livrable doit être constructible en suivant la méthode suivante, et
ne retourner aucune erreur :
\begin{lstlisting}
autoreconf --install --force --verbose
./configure
make
make check
make distcheck
make dist
\end{lstlisting}

Par ailleurs, mettre à disposition le \texttt{tarball} généré par la
commande \texttt{make dist} en tant qu'\emph{artifact} de \gls{CI}
dans le projet sur la plateforme de travail collaboratif, pour tous
les \emph{tags} seulement.

\clearpage%
\part{Couverture du référentiel de certification
  \gls{SIO}}\label{sec:couverturesio}

Ce \gls{PPE} permet d'aborder ou de couvrir les activités et
compétences suivantes :
\begin{enumerate}
  \item [\textbf{A1.1.1}] Analyse du cahier des charges d'un service à
  produire
  \item [\textbf{A1.2.2}] Rédaction des spécifications techniques de
  la solution retenue
  \item [\textbf{A1.2.4}] Détermination des tests nécessaires à la
  validation d'un service
  \item [\textbf{A1.3.4}] Déploiement d'un service
  \item [\textbf{A1.4.1}] Participation à un projet
  \item [\textbf{A1.4.2}] Évaluation des indicateurs de suivi d'un
  projet et justification des écarts
  \item [\textbf{C4.1.1.1}] Identifier les composants logiciels
  nécessaires à la conception de la solution
  \item [\textbf{A4.1.2}] Conception ou adaptation de l'interface
  utilisateur d'une solution application
  \item [\textbf{C4.1.3.2}] Implémenter le schéma de données dans un
  SGBD
  \item [\textbf{C4.1.3.4}] Manipuler les données liées à la solution
  applicative à travers un langage de requête
  \item [\textbf{A4.1.5}] Prototypage de composants logiciels
  \item [\textbf{A4.1.6}] Gestion d'environnements de dévelopement et
  de test
  \item [\textbf{A4.1.7}] Développement, utilisation ou adaptation de
  composants logiciels
  \item [\textbf{A4.1.9}] Rédaction d'une documentation technique
  \item [\textbf{A4.1.10}] Rédaction d'une documentation d'utilisation
  \item [\textbf{A5.2.1}] Exploitation des référentiels, normes et
  standards adoptés par le prestataire informatique (ici le
  commanditaire)
  \item [\textbf{A5.2.4}] Étude d'une technologie, d'un composant,
  d'un outil ou d'une méthode
\end{enumerate}

\clearpage%
\printglossaries{}

\clearpage%
\include{makechangelog}

\end{document}
